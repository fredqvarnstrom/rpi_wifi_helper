# Raspberry Pi WiFi Helper.

## Scenario
    
RPi's BT module will act as iBeacon.

It will broadcast its ip address of `wlan0` interface at the end of 6 bytes.
    
    6832FB68-808D-4A01-AFD8-192168001127
    
    Current IP: 192.168.1.127
     
If RPi is not connected to any router, it will act as WiFi hotspot and broadcast it's IP.

SSID is `RPi3-AP` and password is `raspberry pi`, and IP address is 172.24.1.1

Users can connect to that AP and will visit the web page hosted on RPi. (http://172.24.1.1)

In that page, user will select a AP and input password, and connect to it.


## Working with Bluetooth

### Install dependencies
    
    sudo apt-get install -y libusb-dev 
    sudo apt-get install -y libdbus-1-dev 
    sudo apt-get install -y libglib2.0-dev --fix-missing
    sudo apt-get install -y libudev-dev 
    sudo apt-get install -y libical-dev
    sudo apt-get install -y libreadline-dev
    
### Test iBeacon Broadcasting
    
- Run following commands to check bt module
    
        sudo hciconfig hci0 up
        sudo hciconfig hci0 noleadv
        sudo hciconfig hci0 noscan
        sudo hciconfig hci0 pscan
        sudo hciconfig hci0 leadv

- Start iBeacon
    
        sudo hcitool -i hci0 cmd 0x08 0x0008 1E 02 01 1A 1A FF 4C 00 02 15 63 6F 3F 8F 64 91 4B EE 95 F7 D8 CC 64 A8 63 B5 00 00 00 00 C8 00
        sudo hcitool -i hci0 cmd 0x08 0x0006 A0 00 A0 00 00 00 00 00 00 00 00 00 00 07 00
        sudo hcitool -i hci0 cmd 0x08 0x000a 01

Install **Beacon Tool** app on your Android phone, and you will something about the iBeacon we built above.


## Working with wifi.
    
### Install dependencies
    
        sudo easy_install --upgrade pip
        sudo pip install wifi
        sudo pip install flask
        
### Build WiFi hotspot on RPi 3

    sudo ./setup_wifi_ap.sh

NOTE: We assigned SSID & PASSWORD as `RPi3-AP` & `raspberry`.

- Disable auto-start of `hostapd` service.
    
    We do not need to start `hostapd` service automatically.
    
    We need to run it only when RPi fails to connect to ap.
    
        sudo apt-get install rcconf
        sudo rcconf
    
    Find **hostapd** and disable start-up option.
    
    Press tab, then enter to confirm selections

- Sequence of starting AP
        
        sudo ifdown wlan0
        sudo cp /home/pi/helper/interfaces_ap /etc/network/interfaces
        sudo ifup wlan0        
        sudo service dnsmasq restart
        sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf

## Enable auto-start.
    
Open the `/etc/rc.local` with `sudo nano /etc/rc.local`.

Add following before `exit 0`
    
    (sleep 10; /usr/bin/python /home/pi/rpi_wifi_helper/main.py)&